// import { hanhtrangsoClient } from "configs/api";
import { query } from "../../../helpers/QueryHelper";
import _ from "lodash";

// export async function register(hanhtrangsoUser) {
//   // console.log('🚀', hanhtrangsoUser);

//   const { email, password } = hanhtrangsoUser;
//   const fullName = `${hanhtrangsoUser.firstName} ${hanhtrangsoUser.lastName}`;
//   const roleId = hanhtrangsoUser.roleCode === "PUPIL" ? 1 : 2;
//   const { schoolTypeId } = hanhtrangsoUser;
//   const { city: cityId } = hanhtrangsoUser;
//   const { phoneNumber: phone } = hanhtrangsoUser;

//   // console.log('🚀 hanhtrangsoUser', hanhtrangsoUser);

//   try {
//     const registeredUser = await queryFirst(
//       "p_SACHSO_Users_Register",
//       {
//         email,
//         password: generateSHA1(password),
//         fullName,
//         roleId,
//         schoolTypeId,
//         cityId,
//         phone,
//       },
//       "SachSo"
//     );
//     return registeredUser;
//   } catch (error) {
//     throw new Error(error);
//   }
// }

export async function login(username = "", password = "") {
  try {
    const loggedInUser = await query("Login", {
      username: username,
      password: password,
    });

    const finalUser = {
      ...loggedInUser,
      accessToken: "",
      refreshToken: "",
      password,
    };
    if (!loggedInUser) return undefined;

    if (_.find(loggedInUser.roles, (role) => role.name === "user")) {
      finalUser.role = "user";
    } else if (_.find(loggedInUser.roles, (role) => role.name === "admin")) {
      finalUser.role = "admin";
    }

    return finalUser;
  } catch (error) {
    throw new Error(error);
  }
}
