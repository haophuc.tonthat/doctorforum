import { notification } from "antd";
import { put, takeLatest } from "redux-saga/effects";
import * as actionTypes from "../actions/types";
import { login } from "../services";

const alert = () => {
  notification.error({
    message: "The account does not exist or the password is wrong!",
  });
};

// SSO
function* AUTH_LOGIN(action) {
  try {
    const { email, password } = action.payload;
    const loggedInUser = yield login(email, password);
    if (loggedInUser) {
      yield put({
        type: actionTypes.AUTH_LOGIN_SUCCESS,
        payload: loggedInUser,
      });
    } else {
      alert();
      yield put({ type: actionTypes.AUTH_LOGIN_FAILURE, payload: null });
    }
  } catch (error) {
    alert();
    yield put({ type: actionTypes.AUTH_LOGIN_FAILURE, payload: error });
  }
}

export default function* sagasLogin() {
  yield takeLatest(actionTypes.AUTH_LOGIN, AUTH_LOGIN);
}
