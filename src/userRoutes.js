import Alternative from "views/pages/dashboards/Alternative";
import Forum from "views/pages/user/forum/Forum.js";
import PostContent from "views/pages/user/post/PostContent";
const routes = [
  {
    collapse: true,
    name: "Forum",
    icon: "ni ni-shop text-primary",
    state: "dashboardsCollapse",
    views: [
      {
        path: "/pharmacy",
        name: "Pharmacy",
        miniName: "f",
        component: Forum,
        layout: "/user",
      },
      {
        path: "/b",
        name: "Health",
        miniName: "A",
        component: Alternative,
        layout: "/user",
      },
    ],
  },
];
export default routes;
