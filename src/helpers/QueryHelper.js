import axios from "axios";
function query(proc, parameters = {}) {
  return new Promise((resolve, reject) => {
    axios
      .post(`https://localhost:7289/api/SP/${proc}`, parameters)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export { query };
