import Link from "antd/es/typography/Link";
import CardsHeader from "components/Headers/CardsHeader";
import { query } from "helpers/QueryHelper";
import _ from "lodash";
import moment from "moment";
import React from "react";
import { useParams } from "react-router";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Form,
  Input,
  Media,
  Row,
  UncontrolledTooltip,
} from "reactstrap";

const PostContent = () => {
  const { id } = useParams<any>();
  var currentLocation = window.location;
  const currentTopic = _.split(currentLocation.pathname, "/")[2];

  const [post, setPost] = React.useState<any>([]);
  const [postComments, setPostComments] = React.useState<any>([]);

  const GetPostById = async (topic: any, id: number) => {
    return await query("GetPostById", {
      topic: topic,
      id: id,
    });
  };

  const GetPostCommentList = async (topic: any, postId: number) => {
    return await query("GetPostCommentList", {
      topic: topic,
      postId: postId,
    });
  };

  React.useEffect(() => {
    GetPostById(currentTopic, id).then((res) => {
      setPost(res?.[0]);
    });
    GetPostCommentList(currentTopic, id).then((res) => {
      console.log(res);
      setPostComments(res);
    });
  }, [currentTopic, id]);

  return (
    <>
      <CardsHeader name="Default" parentName="Dashboards" />
      <Container className="mt--6" fluid>
        <Row>
          <Col xl="12">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">{post?.title}</h5>
              </CardHeader>
              <CardHeader className="d-flex align-items-center">
                <div className="d-flex align-items-center">
                  <a href="#pablo" onClick={(e) => e.preventDefault()}>
                    <img
                      alt="..."
                      className="avatar"
                      src={require("assets/img/theme/team-1.jpg").default}
                    />
                  </a>
                  <div className="mx-3">
                    <Link
                      className="text-dark font-weight-600 text-sm"
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                    >
                      {post?.fullName}
                    </Link>
                    <small className="d-block text-muted">
                      {moment(post?.createAt).format("MM/DD/YYYY HH:mm:ss")}
                    </small>
                  </div>
                </div>
                <div className="text-right ml-auto">
                  <Button
                    className="btn-icon"
                    color="primary"
                    size="sm"
                    type="button"
                  >
                    <span className="btn-inner--icon mr-1">
                      <i className="ni ni-fat-add" />
                    </span>
                    <span className="btn-inner--text">Follow</span>
                  </Button>
                </div>
              </CardHeader>
              <CardBody>
                <div
                  className="text-sm"
                  dangerouslySetInnerHTML={{ __html: post?.question }}
                />
                <Row className="align-items-center my-3 pb-3 border-bottom">
                  <Col sm="6">
                    <div className="icon-actions">
                      <a href="#pablo" onClick={(e) => e.preventDefault()}>
                        <i className="ni ni-chat-round" />
                        <span className="text-muted">36</span>
                      </a>
                      <a href="#pablo" onClick={(e) => e.preventDefault()}>
                        <i className="ni ni-curved-next" />
                        <span className="text-muted">12</span>
                      </a>
                    </div>
                  </Col>
                  <Col className="d-none d-sm-block" sm="6">
                    <div className="d-flex align-items-center justify-content-sm-end">
                      <div className="avatar-group">
                        <a
                          className="avatar avatar-xs rounded-circle"
                          href="#pablo"
                          id="tooltip36177092"
                          onClick={(e) => e.preventDefault()}
                        >
                          <img
                            alt="..."
                            src={require("assets/img/theme/team-1.jpg").default}
                          />
                        </a>
                        <UncontrolledTooltip delay={0} target="tooltip36177092">
                          Jessica Rowland
                        </UncontrolledTooltip>
                        <a
                          className="avatar avatar-xs rounded-circle"
                          href="#pablo"
                          id="tooltip857639221"
                          onClick={(e) => e.preventDefault()}
                        >
                          <img
                            alt="..."
                            className="rounded-circle"
                            src={require("assets/img/theme/team-2.jpg").default}
                          />
                        </a>
                        <UncontrolledTooltip
                          delay={0}
                          target="tooltip857639221"
                        >
                          Audrey Love
                        </UncontrolledTooltip>
                        <a
                          className="avatar avatar-xs rounded-circle"
                          href="#pablo"
                          id="tooltip260223080"
                          onClick={(e) => e.preventDefault()}
                        >
                          <img
                            alt="..."
                            className="rounded-circle"
                            src={require("assets/img/theme/team-3.jpg").default}
                          />
                        </a>
                        <UncontrolledTooltip
                          delay={0}
                          target="tooltip260223080"
                        >
                          Michael Lewis
                        </UncontrolledTooltip>
                      </div>
                      <small className="pl-2 font-weight-bold">
                        and 30+ more
                      </small>
                    </div>
                  </Col>
                </Row>

                <div className="mb-1">
                  {postComments.map((item: any, index: number) => {
                    return (
                      <Media className="media-comment" key={index}>
                        <img
                          alt="..."
                          className="avatar avatar-lg media-comment-avatar rounded-circle"
                          src={require("assets/img/theme/team-1.jpg").default}
                        />
                        <Media>
                          <div className="media-comment-text">
                            <h6 className="h5 mt-0">{item?.fullName}</h6>
                            <p className="text-sm lh-160">{item?.comment}</p>
                            <div className="icon-actions">
                              <a
                                className="like active"
                                href="#pablo"
                                onClick={(e) => e.preventDefault()}
                              >
                                <i className="ni ni-like-2" />
                                <span className="text-muted">3 likes</span>
                              </a>
                              <a
                                href="#pablo"
                                onClick={(e) => e.preventDefault()}
                              >
                                <i className="ni ni-curved-next" />
                                <span className="text-muted">2 shares</span>
                              </a>
                            </div>
                          </div>
                        </Media>
                      </Media>
                    );
                  })}
                  <hr />
                  <Media className="align-items-center">
                    <img
                      alt="..."
                      className="avatar avatar-lg rounded-circle mr-4"
                      src={require("assets/img/theme/team-3.jpg").default}
                    />
                    <Media body>
                      <Form>
                        <Input
                          placeholder="Write your comment"
                          rows="1"
                          type="textarea"
                        />
                      </Form>
                    </Media>
                  </Media>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default PostContent;
