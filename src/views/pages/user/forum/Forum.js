import React from "react";
import { Card, CardHeader, Col, Container, Row } from "reactstrap";

// core components
import CardsHeader from "components/Headers/CardsHeader.js";

import { query } from "helpers/QueryHelper";
import _ from "lodash";
import PostItem from "views/pages/components/PostItem";

function Forum() {
  var currentLocation = window.location;
  const currentTopic = _.trim(currentLocation.pathname, "/user/");

  const capitalize = (str) => `${str.charAt(0).toUpperCase()}${str.slice(1)}`;

  const [post, setPost] = React.useState([]);

  const GetPostByTopic = async (topic) => {
    return await query("GetPostByTopic", {
      topic: topic,
    });
  };

  React.useEffect(() => {
    GetPostByTopic(currentTopic).then((res) => {
      setPost(res);
    });
  }, [currentTopic]);

  return (
    <>
      <CardsHeader name="Default" parentName="Dashboards" />
      <Container className="mt--6" fluid>
        <Row>
          <Col xl="12">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">{capitalize(currentTopic)}</h5>
              </CardHeader>
              {post.map((item, index) => (
                <PostItem
                  key={index}
                  currentTopic={currentTopic}
                  data={item}
                ></PostItem>
              ))}
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Forum;
