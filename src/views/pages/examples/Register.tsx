import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Col,
} from "reactstrap";
// core components
import AuthHeader from "components/Headers/AuthHeader.js";
import { Button, Form, Input, notification } from "antd";
import FormItem from "antd/es/form/FormItem";
import { query } from "helpers/QueryHelper";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { login } from "modules/Auth/actions";
import { init } from "modules/Auth/actions";

function Register() {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const history = useHistory();
  const loggedInUser = useSelector(
    (userState: any) => userState.authReducer.loggedInUser
  );

  React.useEffect(() => {
    if (loggedInUser) {
      history.push(`/user`);
    } else {
      dispatch(init());
    }
  }, [dispatch, history, loggedInUser]);

  const handleSubmit = async (values: any) => {
    await query("Register", {
      fullName: values.fullname,
      email: values.email,
      phoneNumber: values.phone,
      user: {
        username: values.username,
        password: values.password,
      },
    })
      .then(() => {
        notification.success({
          message: "Register Success, Login in 3 seconds!",
        });
        setTimeout(() => {
          dispatch(login(values.username, values.password));
        }, 3000);
      })
      .catch(() => {
        notification.error({
          message: "Server Error!",
        });
      });
  };
  return (
    <>
      <AuthHeader
        title="Create an account"
        lead="Use these awesome forms to login or create new account in your project for free."
      />
      <Container className="mt--8 pb-5">
        <Row className="justify-content-center">
          <Col lg="6" md="8">
            <Card className="bg-secondary border-0">
              <CardBody className="px-lg-5 py-lg-5">
                <Form
                  form={form}
                  name="register"
                  onFinish={handleSubmit}
                  style={{ maxWidth: 600 }}
                  scrollToFirstError
                  autoComplete="off"
                >
                  <Form.Item
                    name="fullname"
                    tooltip="What do you want others to call you?"
                    rules={[
                      {
                        required: true,
                        message: "Please input your nickname!",
                        whitespace: true,
                      },
                    ]}
                  >
                    <Input
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-user"
                        />
                      }
                      placeholder="Full Name (*)"
                    />
                  </Form.Item>
                  <Form.Item
                    name="username"
                    rules={[
                      {
                        min: 5,
                        message: "Username must be minimum 5 characters.",
                      },
                      {
                        required: true,
                        message: "Please input your E-mail!",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-user"
                        />
                      }
                      placeholder="Username (*)"
                    />
                  </Form.Item>

                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-lock"
                        />
                      }
                      placeholder="Password (*)"
                    />
                  </Form.Item>

                  <Form.Item
                    name="confirm"
                    dependencies={["password"]}
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: "Please confirm your password!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("password") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error(
                              "The two passwords that you entered do not match!"
                            )
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-lock"
                        />
                      }
                      placeholder="Confirm Password (*)"
                    />
                  </Form.Item>

                  <Form.Item
                    name="email"
                    rules={[
                      {
                        type: "email",
                        message: "The input is not valid E-mail!",
                      },
                      {
                        required: true,
                        message: "Please input your E-mail!",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-envelope"
                        />
                      }
                      placeholder="Email (*)"
                    />
                  </Form.Item>

                  <Form.Item
                    name="phone"
                    rules={[
                      {
                        required: true,
                        message: "The input is not valid phone number!",
                        pattern: new RegExp(/^[0-9]+$/),
                      },
                      {
                        required: true,
                        message: "Please input your phone number!",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-phone"
                        />
                      }
                      placeholder="Phone Number (*)"
                    />
                  </Form.Item>
                  <div className="text-center">
                    <Button
                      className="mt-2"
                      type="primary"
                      htmlType="submit"
                      size="large"
                      block
                    >
                      Register
                    </Button>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Register;
