// reactstrap components
import { Card, CardBody, Container, Row, Col } from "reactstrap";
// core components
import AuthHeader from "components/Headers/AuthHeader.js";
import { init, login } from "modules/Auth/actions";
import { Button, Form, Input } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
const FormItem = Form.Item;
function Login() {
  const [form] = Form.useForm();
  const loggedInUser = useSelector(
    (userState: any) => userState.authReducer.loggedInUser
  );
  const dispatch = useDispatch();
  const history = useHistory();
  React.useEffect(() => {
    if (loggedInUser) {
      if (loggedInUser.role == "Admin") {
        history.push(`/admin`);
      } else if (loggedInUser.role == "User") {
        history.push(`/user`);
      }
    } else {
      dispatch(init());
    }
  }, [dispatch, history, loggedInUser]);
  const handleSubmit = (values: any) => {
    const { username, password } = values;
    dispatch(login(username, password));
  };
  return (
    <>
      <AuthHeader
        title="Welcome!"
        lead="Use these awesome forms to login or create new account in your project for free."
      />
      <Container className="mt--8 pb-5">
        <Row className="justify-content-center">
          <Col lg="5" md="7">
            <Card className="bg-secondary border-0 mb-0">
              <CardBody className="px-lg-5 py-lg-5">
                <Form autoComplete="off" form={form} onFinish={handleSubmit}>
                  <FormItem
                    hasFeedback
                    name="username"
                    rules={[
                      {
                        whitespace: true,
                        message: "Username is require!",
                      },
                      {
                        required: true,
                        message: "Username is require!",
                      },
                    ]}
                  >
                    <Input
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-user"
                        />
                      }
                      placeholder="Username (*)"
                    />
                  </FormItem>

                  <FormItem
                    hasFeedback
                    name="password"
                    rules={[
                      // { min: 6, message: "Tối thiểu 6 ký tự!" },
                      {
                        whitespace: true,
                        message: "Password is require!",
                      },
                      { required: true, message: "Password is require!" },
                    ]}
                  >
                    <Input.Password
                      style={{
                        boxShadow:
                          "0 1px 3px rgba(50, 50, 93, 0.15), 0 1px 0 rgba(0, 0, 0, 0.02)",
                        borderRadius: 3,
                      }}
                      allowClear
                      size="large"
                      prefix={
                        <i
                          style={{ color: "rgba(0,0,0,.25)" }}
                          className="fas fa-lock"
                        />
                      }
                      placeholder="Password (*)"
                    />
                  </FormItem>

                  <div className="text-center">
                    <Button
                      className="mt-2"
                      type="primary"
                      htmlType="submit"
                      size="large"
                      block
                    >
                      Login
                    </Button>
                  </div>
                </Form>
              </CardBody>
            </Card>
            <Row className="mt-3">
              <Col xs="6">
                <a
                  className="text-light"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                >
                  <small>Forgot password?</small>
                </a>
              </Col>
              <Col className="text-right" xs="6">
                <a
                  className="text-light"
                  onClick={(e) => {
                    e.preventDefault();
                    history.push("/register");
                  }}
                >
                  <small>Create new account</small>
                </a>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Login;
