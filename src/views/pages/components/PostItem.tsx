import Link from "antd/es/typography/Link";
import _ from "lodash";
import moment from "moment";
import React from "react";
import { useHistory } from "react-router";
import { CardBody } from "reactstrap";

const PostItem = ({ data, currentTopic }: any) => {
  const history = useHistory();
  const handleClickPost = () => {
    history.push(`${currentTopic}/post/${data.id}`);
  };
  return (
    <div className="border">
      <CardBody className="pt-4 pb-0 ">
        <Link className=" text-lg font-weight-bold" onClick={handleClickPost}>
          {data.title}
        </Link>
        <div
          className="text-sm"
          dangerouslySetInnerHTML={{ __html: data.question }}
        />
      </CardBody>
      <CardBody className="d-flex align-items-center">
        <div className="d-flex align-items-center ">
          <a href="#pablo" onClick={(e) => e.preventDefault()}>
            <img
              alt="..."
              className="avatar"
              src={require("assets/img/theme/team-1.jpg").default}
            />
          </a>
          <div className="mx-3">
            <Link
              className="font-weight-600 text-sm"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              {data?.fullName}
            </Link>
            <small className="d-block text-muted">
              {moment(data?.createAt).format("MM/DD/YYYY HH:mm:ss")}
            </small>
          </div>
        </div>
      </CardBody>
    </div>
  );
};

export default PostItem;
